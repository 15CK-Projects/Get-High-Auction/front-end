import * as config from "./utils/config.js";
import { formDataToJSON, formDataToObject } from "./utils/form.js";

$(function () {
    document.getElementById("brand").setAttribute("href", config.contextPath);
    
    $("#loginForm").submit(async function (event) {
        event.preventDefault();

        if (!$(this).valid()) {
            return;
        }

        let data = formDataToJSON(this);

        let loginPromise = new Promise((resolve, reject) => {
            $.ajax({
                url: `${config.apiPath}/auth/login`,
                type: "POST",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: data
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await loginPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
            } else if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 400) {
                    swal("Lỗi", "Bạn phải cung cấp tên tài khoản và mật khẩu để đăng nhập.", "error");
                } else if (status === 401) {
                    swal("Lỗi", "Mật khẩu không chính xác.", "error");
                } else if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản với tên được cung cấp.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }
            } else {
                swal("Lỗi", "Oops! Something went wrong!", "error");
            }
            return;
        }

        window.localStorage.setItem("userId", doneData.userId);
        window.localStorage.setItem("accessToken", doneData.accessToken);
        window.localStorage.setItem("expiredAt", doneData.expiredAt);
        window.localStorage.setItem("refreshToken", doneData.refreshToken);
        window.localStorage.setItem("user", JSON.stringify(doneData.user));

        await swal({
            title: "Thông báo",
            text: "Đăng nhập thành công! Chuyển đến trang chủ sau 3s.",
            icon: "success",
            button: false,
            timer: 3000
        });

        window.location.replace(config.contextPath);
    }).validate({
        rules: {
            userName: "required",
            password: "required"
        },
        messages: {
            userName: "Tên tài khoản không được để trống.",
            password: "Mật khẩu không được để trống."
        },
        // Element chứa message báo lỗi chỉ cần điều chỉnh tag gì và class gì.
        // Việc tự động thêm element vào hoặc xóa element đi thì tự động, không cần can thiệp.
        errorElement: "div",
        errorClass: "invalid-feedback",
        // Điều khiển việc high-light và un-high-light đối với input.
        // Sử dụng Bootstrap 4.1, nên theo hướng dẫn trong ví dụ của Bootstrap Documentation.
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });

    $("#registerForm").submit(async function (event) {
        event.preventDefault();

        if (!$(this).valid()) {
            return;
        }

        let data = formDataToObject(this);
        // Loại bỏ thông tin không cần gửi đi trong form.
        // Không cần thiết do Mongoose tự động bỏ qua những field không có trong Schema.
        delete data.confirmPassword;
        delete data["hidden-g-recaptcha"];

        grecaptcha.reset();

        let registerPromise = new Promise((resolve, reject) => {
            $.ajax({
                url: `${config.apiPath}/auth/register`,
                type: "POST",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify(data)
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await registerPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Không thể thiết lập kết nối tới máy chủ.", "error");
            } else if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 400) {
                    swal("Lỗi", "Bạn chưa hoàn thành Google reCAPTCHA.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }
            } else {
                swal("Lỗi", "Oops! Something went wrong!", "error");
            }

            return;
        }

        window.localStorage.setItem("userId", doneData.userId);
        window.localStorage.setItem("accessToken", doneData.accessToken);
        window.localStorage.setItem("expiredAt", doneData.expiredAt);
        window.localStorage.setItem("refreshToken", doneData.refreshToken);
        window.localStorage.setItem("user", JSON.stringify(doneData.user));

        await swal({
            title: "Thông báo",
            text: "Đăng kí thành công! Chuyển đến trang chủ sau 3s.",
            icon: "success",
            button: false,
            timer: 3000
        });

        window.location.replace(config.contextPath);
    }).validate({
        ignore: [], // jQuery validation sẽ validate những input hidden: hidden-g-recaptcha.
        rules: {
            userName: "required",
            password: {
                required: true,
                minlength: 6
            },
            confirmPassword: {
                equalTo: "#registerForm_Password"
            },
            email: {
                required: true,
                email: true
            },
            "hidden-g-recaptcha": {
                required: () => grecaptcha.getResponse() === ""
            }
        },
        messages: {
            userName: "Tên tài khoản không được để trống.",
            password: {
                required: "Mật khẩu không được để trống.",
                minlength: "Mật khẩu phải dài từ 6 kí tự trở lên."
            },
            confirmPassword: {
                required: "Phải xác nhận mật khẩu một lần nữa.",
                equalTo: "Mật khẩu xác nhận không chính xác."
            },
            email: {
                required: "Email đăng ký tài khoản không được để trống.",
                email: "Email không đúng định dạng."
            },
            "hidden-g-recaptcha": {
                required: "Xin vui lòng nhấn vào Google reCAPTCHA."
            }
        },
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });

    $("#resetForm").submit(function (event) {
        event.preventDefault();

    }).validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "Phải nhập email để tiếp tục.",
                email: "Email không đúng định dạng."
            }
        },
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });
});
