import * as config from "./utils/config.js";
import * as helper from "./utils/handlebars-helper.js";
import * as formatter from "./utils/formatter.js";
import * as checker from "./utils/checker.js";

let productId;
let product;

let statisticsTemplate;
let imageInformationBiddingTemplate;
let descriptionTemplate;
let biddingHistoryTemplate;
let publicProfileContentTemplate;

async function loadProduct() {
    if (!productId) {
        return Promise.reject();
    }

    try {
        await checker.reCheckAuthentication();
    } catch (errorData) {
        console.log(errorData);
        return;
    }

    let getProductPromise = new Promise((resolve, reject) => {
        $.ajax({
            url: `${config.apiPath}/product/${productId}`,
            type: "get",
            dataType: "json",
            timeout: config.ajaxTimeout
        }).done((data, textStatus, xhr) => {
            resolve(data)
        }).fail((xhr, textStatus, errorThrown) => {
            reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
        });
    });

    try {
        product = await getProductPromise;
    } catch (errorData) {
        return Promise.reject();
    }
    
    let html = imageInformationBiddingTemplate(product);
    $("#image-information-bidding").empty().append(html);

    html = descriptionTemplate(product.description);
    $("#description").empty().append(html);

    let getBiddingEntriesPromise = new Promise((resolve, reject) => {
        let accessToken = localStorage.getItem("accessToken");
        $.ajax({
            url: `${config.apiPath}/product/${productId}/bidding-entry`,
            headers: { "x-access-token": accessToken },
            type: "get",
            dataType: "json",
            timeout: config.ajaxTimeout
        }).done((data, textStatus, xhr) => {
            resolve(data)
        }).fail((xhr, textStatus, errorThrown) => {
            reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
        });
    });

    let biddingEntries;
    try {
        biddingEntries = await getBiddingEntriesPromise;
    } catch (errorData) {
        console.log(errorData);
        return;
    }

    html = biddingHistoryTemplate(biddingEntries);
    $("#biddingHistory").empty().append(html);

    $("#biddingPriceInput").inputmask({
        alias: "numeric",
        autoUnmask: true, // $("selector").val() => unmask value.
        radixPoint: ",",
        groupSeparator: ".",
        rightAlign: false,
        autoGroup: true
    });
}

$(async function () {
    Handlebars.registerHelper("formatCurrency", helper.formatCurrency);
    Handlebars.registerHelper("formatDateTime", helper.formatDateTime);
    Handlebars.registerHelper("formatNumber", helper.formatNumber);
    Handlebars.registerHelper("math", helper.math);
    Handlebars.registerHelper("isNew", helper.isNew);
    Handlebars.registerHelper("timeLeft", helper.timeLeft);
    Handlebars.registerHelper("getImageByIndex", helper.getImageByIndex);
    Handlebars.registerHelper("buildAbsoluteImagePath", helper.buildAbsoluteImagePath);
    Handlebars.registerHelper("hideStringWithAsterisk", helper.hideStringWithAsterisk);

    statisticsTemplate = Handlebars.compile($("#statistics-template").html());
    imageInformationBiddingTemplate = Handlebars.compile($("#image-information-bidding-template").html());
    descriptionTemplate = Handlebars.compile($("#description-template").html());
    biddingHistoryTemplate = Handlebars.compile($("#biddingHistory-template").html());
    publicProfileContentTemplate = Handlebars.compile($("#publicProfileContent-template").html());

    $("#brand").attr("href", config.contextPath);

    let url = new URL(window.location.href);
    productId = url.searchParams.get("product-id");

    try {
        await loadProduct();
    } catch (errorData) {
        swal("Không thể hiển thị được sản phẩm.");
        console.error(errorData);
        return;
    }

    // Start worker after 10 seconds.
    setInterval(updateChangesWorker, 10000);

    $("#image-information-bidding").on("click", "button[data-type='public-profile']", async function (event) {
        let userId = this.dataset.id;

        let getPromise = new Promise((resolve, reject) => {
            $.ajax({
                url: `${config.apiPath}/user/${userId}/public-profile`,
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let publicProfile;
        try {
            publicProfile = await getPromise;
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let html = publicProfileContentTemplate(publicProfile);
        $("#publicProfileContent").empty().append(html);
        $("#publicProfile").modal();
    });

    $("#image-information-bidding").on("click", "#addBiddingButton", async function (event) {
        let biddingPrice = $("#biddingPriceInput").val();
        biddingPrice = Number.parseInt(biddingPrice, 10);
        if (Number.isNaN(biddingPrice)) {
            swal("Giá đặt không hợp lệ, xin mời nhập lại.");
            return;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let confirmed = await swal({
            title: "Bạn có chắc chắn?",
            text: `Giá đặt cho sản phẩm là: ${formatter.currencyFormatter.format(biddingPrice)}`,
            icon: "warning",
            buttons: {
                cancel: {
                    text: "Hủy",
                    value: false,
                    visible: true
                },
                confirm: {
                    text: "Xác nhận",
                    value: true,
                    visible: true
                }
            },
            dangerMode: true
        });

        if (!confirmed) {
            return;
        }

        let addBiddingPromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/product/${productId}/bidding-entry`,
                headers: { "x-access-token": accessToken },
                type: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify({ product: productId, price: biddingPrice })
            }).done((data, textStatus, xhr) => {
                resolve({ data: data, textStatus: textStatus, xhr: xhr });
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await addBiddingPromise;
            console.log(doneData);
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 404) {
                    let type = errorData.xhr.responseJSON.type;

                    if (type === 1) {
                        swal("Lỗi", "Sản phẩm không tồn tại.", "error");
                        return;
                    }

                    if (type === 2) {
                        swal("Lỗi", "Sản phẩm đã hết hạn đấu giá.", "error");
                        return;
                    }
                }
                if (status === 422) {
                    let type = errorData.xhr.responseJSON.type;

                    if (type === 1) {
                        swal({
                            title: "Thông báo",
                            text: "Bạn đã bị kich.",
                            icon: "error"
                        });
                        return;
                    }

                    if (type === 2) {
                        swal({
                            title: "Thông báo",
                            text: "Giá đưa ra không cao hơn giá hiện tại.",
                            icon: "error"
                        });
                        return;
                    }

                    if (type === 3) {
                        swal({
                            title: "Thông báo",
                            text: "Giá đưa ra không phải là bội của bước giá.",
                            icon: "error"
                        });
                        return;
                    }
                }
            }

            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        swal({
            title: "Thông báo",
            text: "Hệ thống đã ghi nhận yêu cầu đặt giá của bạn. Việc cập nhật có thể diễn ra tới hơn 10 phút. Bạn cần tải lại trang để xem được cập nhật.",
            icon: "success",
            button: false,
            timer: 5000
        });
    }).on("click", "#reloadProductButton", async function (event) {
        try {
            await loadProduct();
        } catch (errorData) {
            swal("Không thể hiển thị được sản phẩm.");
            console.error(errorData);
            return;
        }
    });

    let userJSON = localStorage.getItem("user");
    let userId = localStorage.getItem("userId");
    if (userJSON && userId) {
        let user = JSON.parse(userJSON);

        if (user.roles.indexOf("seller") !== -1 && userId === product.seller._id) {
            $("#summernote").summernote({
                placeholder: "Mô tả thêm cho sản phẩm",
                tabsize: 2,
                height: 200
            });
            $("#summernote").summernote("code", "");
            document.getElementById("sellerFunctions").hidden = false;
        }
    }

    $("#saveDescriptionButton").on("click", async function (event) {
        let editor = $("#summernote");
        let data = {
            content: editor.summernote("code")
        };

        if (!data.content || data.content === "") {
            swal("Thông báo", "Nội dung mô tả không được để trống.", "warning");
            return;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let postPromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/product/${productId}/description`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify(data)
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        try {
            await postPromise;
        } catch (errorData) {
            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        editor.summernote("code", "");
        swal("OK", "Đã thêm mô tả.", "success");
    });

    $("#banHighestBiddingUser").on("click", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let postPromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/product/${productId}/ban`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        try {
            await postPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }
            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 422) {
                    swal("OK", "Sản phẩm không có ai đặt giá.", "warning");
                    return;
                }

                swal("Lỗi", "Something went wrong!", "error");
                return;
            }

            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        swal("OK", "Người đặt giá cao nhất đã bị kick.", "success");
    });
});

async function updateChangesWorker() {
    let getPromise = new Promise((resolve, reject) => {
        $.ajax({
            url: `${config.apiPath}/product/${productId}`,
            type: "get",
            dataType: "json",
            timeout: config.ajaxTimeout
        }).done((data, textStatus, jqXHR) => {
            resolve(data);
        }).fail((xhr, textStatus, errorThrown) => {
            reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
        });
    });

    let product;
    try {
        product = await getPromise;
    } catch (errorData) {
        console.log(errorData);
        return;
    }

    let html = statisticsTemplate(product);
    $("#productStatistics").empty().append(html);
}
