let currencyFormatter = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
    minimumFractionDigits: 0
});

let options = {
    year: 'numeric', month: '2-digit', day: '2-digit',
    hour: '2-digit', minute: '2-digit', second: '2-digit',
    timeZone: "Asia/Ho_Chi_Minh"
}

let dateTimeFormatter = new Intl.DateTimeFormat("vi-VN", options);

let numberFormatter = new Intl.NumberFormat("vi-VN", {
    style: "decimal"
});

export { currencyFormatter, dateTimeFormatter, numberFormatter }
