import * as formatter from "./formatter.js";
import * as config from "./config.js";

let formatCurrency = function (value) {
    return formatter.currencyFormatter.format(value);
}

let formatDateTime = function (value) {
    return formatter.dateTimeFormatter.format(new Date(value));
}

let formatNumber = function (value) {
    return formatter.numberFormatter.format(value);
}

let math = function (leftOperand, operator, rightOperand) {
    leftOperand = Number.parseFloat(leftOperand);
    rightOperand = Number.parseFloat(rightOperand);

    return {
        "+": leftOperand + rightOperand,
        "-": leftOperand - rightOperand,
        "*": leftOperand * rightOperand,
        "/": leftOperand / rightOperand,
        "%": leftOperand % rightOperand
    }[operator];
}

let timeLeft = function (dateTime) {
    return moment(dateTime).locale("vi-VN").fromNow();
}

let getImageByIndex = function (imageArray, index) {
    if (!imageArray || imageArray.length === 0) {
        return "";
    }

    if (index < 0 || index >= imageArray.length) {
        return "";
    }

    return imageArray[index];
}

let buildAbsoluteImagePath = function (imageName) {
    if (!imageName || imageName === "") {
        return "#";
    }

    return `${config.apiPath}/public/images/${imageName}`;
}

let isNew = function (startTime) {
    let diff = moment().diff(moment(startTime), "minutes");
    if (diff <= 60) {
        return true;
    }
    return false;
}

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

let hideStringWithAsterisk = function (text) {
    let hiddenText = "";
    for (let i = 0; i < text.length; ++i) {
        if (i % 2 !== 0) {
            hiddenText += "*";
        } else {
            hiddenText += text[i];
        }
    }
    return hiddenText;
}

export { formatCurrency, formatDateTime, formatNumber, math, isNew, timeLeft, getImageByIndex, buildAbsoluteImagePath, hideStringWithAsterisk }
