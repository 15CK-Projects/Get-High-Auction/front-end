function formDataToObject(formElement) {
    let formData = new FormData(formElement);

    let javaScriptObject = {};
    for (let pair of formData.entries()) {
        javaScriptObject[pair[0]] = pair[1];
    }
    
    return javaScriptObject;
}

function formDataToJSON(formElement) {
    let javaScriptObject= formDataToObject(formElement);

    return JSON.stringify(javaScriptObject);    
}

export {formDataToObject, formDataToJSON};
