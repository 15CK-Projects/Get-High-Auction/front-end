import * as config from "../utils/config.js";

async function reCheckAuthentication() {
    let userId = localStorage.getItem("userId");
    let accessToken = localStorage.getItem("accessToken");
    let refreshToken = localStorage.getItem("refreshToken");
    let expiredAt = localStorage.getItem("expiredAt");

    if (!userId || !refreshToken || !accessToken || !expiredAt) {
        window.location.replace(`${config.contextPath}/sign-in.html`);
        return Promise.reject("Missing local storage field.");
    }

    expiredAt = new Date(Number.parseInt(expiredAt)); // Parse number to Date.
    let difference = Date.now() - expiredAt;
    let differenceMinutes = Math.ceil(Math.abs(difference) / 1000 / 60);

    if (difference < 0 && differenceMinutes >= 5) {
        return Promise.resolve("OK");
    }

    let getNewAccessTokenPromise = new Promise((resolve, reject) => {
        $.ajax({
            url: `${config.apiPath}/auth/token`,
            type: "POST",
            dataType: "json",
            timeout: config.ajaxTimeout,
            contentType: "application/json",
            data: JSON.stringify({ userId: userId, refreshToken: refreshToken })
        }).done((data, textStatus, xhr) => {
            resolve({ data: data, textStatus: textStatus, xhr: xhr });
        }).fail((xhr, textStatus, errorThrown) => {
            reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
        });
    })

    let doneData;
    try {
        doneData = await getNewAccessTokenPromise;
    } catch (errorData) {
        if (errorData.textStatus === "timeout") {
            return Promise.reject("Request timeout");
        }
        if (errorData.textStatus === "error") {
            window.location.replace(`${config.contextPath}/sign-in.html`);
            return Promise.reject(errorData);
        }
    }

    window.localStorage.setItem("userId", doneData.data.userId);
    window.localStorage.setItem("accessToken", doneData.data.accessToken);
    window.localStorage.setItem("expiredAt", doneData.data.expiredAt);
    window.localStorage.setItem("refreshToken", doneData.data.refreshToken);
    window.localStorage.setItem("user", JSON.stringify(doneData.data.user));

    // Không cần return Promise.resolve()
    // Vì async function ngầm định trả về Promise.resolve() nếu như hàm chạy bình thường.
}

export { reCheckAuthentication };
