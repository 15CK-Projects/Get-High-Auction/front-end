import * as config from "./utils/config.js";
import * as helper from "./utils/handlebars-helper.js";

$(async function () {
    let url = new URL(window.location.href);
    let userId = url.searchParams.get("user-id");

    if (!userId) {
        window.location.replace(config.contextPath);
        return;
    }

    document.getElementById("brand").setAttribute("href", config.contextPath);

    Handlebars.registerHelper("formatCurrency", helper.formatCurrency);
    Handlebars.registerHelper("formatDateTime", helper.formatDateTime);
    Handlebars.registerHelper("formatNumber", helper.formatNumber);
    Handlebars.registerHelper("math", helper.math);
    Handlebars.registerHelper("isNew", helper.isNew);
    Handlebars.registerHelper("timeLeft", helper.timeLeft);
    Handlebars.registerHelper("getImageByIndex", helper.getImageByIndex);
    Handlebars.registerHelper("buildAbsoluteImagePath", helper.buildAbsoluteImagePath);
    Handlebars.registerHelper("hideStringWithAsterisk", helper.hideStringWithAsterisk);

    let publicProfileContentTemplate = Handlebars.compile($("#publicProfileContent-template").html());

    let getPromise = new Promise((resolve, reject) => {
        $.ajax({
            url: `${config.apiPath}/user/${userId}/public-profile`,
            method: "get",
            dataType: "json",
            timeout: config.ajaxTimeout
        }).done((data, textStatus, xhr) => {
            resolve(data);
        }).fail((xhr, textStatus, errorThrown) => {
            reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
        });
    });

    let publicProfile;
    try {
        publicProfile = await getPromise;
    } catch (errorData) {
        console.log(errorData);
        return;
    }

    let html = publicProfileContentTemplate(publicProfile);
    $("#content").empty().append(html);
});
