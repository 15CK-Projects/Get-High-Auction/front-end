import * as config from "./utils/config.js";
import * as helper from "./utils/handlebars-helper.js";
import * as formatter from "./utils/formatter.js";
import * as formUtil from "./utils/form.js";
import * as checker from "./utils/checker.js";

$(async function () {
    document.getElementById("brand").setAttribute("href", config.contextPath);

    Handlebars.registerHelper("formatCurrency", helper.formatCurrency);
    Handlebars.registerHelper("formatDateTime", helper.formatDateTime);
    Handlebars.registerHelper("math", helper.math);

    let watchListItemTemplate = Handlebars.compile($("#watchListItem-template").html());
    let biddingProductTemplate = Handlebars.compile($("#biddingProduct-template").html());
    let winningProductTemplate = Handlebars.compile($("#winningProduct-template").html());
    let reviewTemplate = Handlebars.compile($("#review-template").html());
    let sellingProductTemplate = Handlebars.compile($("#sellingProduct-template").html());
    let soldProductTemplate = Handlebars.compile($("#soldProduct-template").html());
    let categoryListTemplate = Handlebars.compile($("#categoryList-template").html());
    let userListTemplate = Handlebars.compile($("#userList-template").html());
    let userListRowTemplate = Handlebars.compile($("#userListRow-template").html());
    let upgradeRequestListTemplate = Handlebars.compile($("#upgradeRequestList-template").html());

    $("#brand").attr("href", config.contextPath);

    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
        if (user.roles.indexOf("seller") !== -1) {
            document.getElementById("v-pills-addNewProduct-tab").hidden = false;
            document.getElementById("v-pills-sellingProduct-tab").hidden = false;
            document.getElementById("v-pills-soldProduct-tab").hidden = false;
        }

        if (user.roles.indexOf("admin") !== -1) {
            document.getElementById("v-pills-upgradeRequest-tab").hidden = false;
            document.getElementById("v-pills-userManagement-tab").hidden = false;
            document.getElementById("v-pills-categoryManagement-tab").hidden = false;
        }
    }

    // Thông tin cá nhân
    $("#v-pills-personalInformations-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let getUserPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");

            $.ajax({
                url: `${config.apiPath}/user/${userId}`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let userData;
        try {
            userData = await getUserPromise;
        } catch (errorData) {
            swal("Lỗi", "Something went wrong!", "error");
            console.log(errorData);
            return;
        }

        $("#userName").val(userData.userName);
        $("#lastName").val(userData.lastName);
        $("#firstName").val(userData.firstName);
        $("#email").val(userData.email);
        $("#address").val(userData.address);
    }).trigger("show.bs.tab"); // Default tab, trigger event to show informations.

    $("#personalInformationsForm").on("submit", async function (event) {
        event.preventDefault();

        if (!$(this).valid()) {
            return;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let data = formUtil.formDataToJSON(this);

        let updatePromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}`,
                headers: { "x-access-token": accessToken },
                method: "put",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: data
            }).done((data, textStatus, xhr) => {
                resolve({ data: data, textStatus: textStatus, xhr: xhr });
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await updatePromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
            } else if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền cập nhật thông tin tài khoản khác.", "error");
                } else if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }
            } else {
                swal("Lỗi", "Oops! Something went wrong!", "error");
            }
            return;
        }

        localStorage.setItem("user", JSON.stringify({ userName: doneData.data.userName, roles: doneData.data.roles }));

        swal("Thông báo", "Thông tin của bạn đã được cập nhật thành công.", "success");
    }).validate({
        rules: {
            userName: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            userName: "Tên tên tài khoản không được để trống.",
            email: {
                required: "Email không được để trống.",
                email: "Email không hợp lệ, vui lòng kiểm tra lại."
            }
        },
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });

    $("#upgradeRequest-button").on("click", async function (event) {
        let postPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/upgrade-request`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let response;
        try {
            response = await postPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;

                if (status === 422) {
                    let type = errorData.xhr.responseJSON.type;
                    if (type === 1) {
                        swal("Thông báo", "Tài khoản của bạn đã được nâng cấp. Không cần phải yêu cầu duyệt.", "success");
                        return;
                    }

                    if (type === 2) {
                        swal("Thông báo", "Yêu cầu nâng cấp đang chờ duyệt.", "warning");
                        return;
                    }
                }
            }

            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        swal("Thông báo", "Yêu cầu nâng cấp tài khoản đã được ghi nhận.", "success");
    });
    // ====

    // Thay đổi mật khẩu
    $("#v-pills-changePassword-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            console.log(errorData);
            return;
        }
    });

    $("#changePasswordForm").on("submit", async function (event) {
        event.preventDefault();

        if (!$(this).valid()) {
            return;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let data = formUtil.formDataToJSON(this);

        let changePromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/password`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: data
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        try {
            await changePromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 401) {
                    swal("Lỗi", "Mật khẩu cũ không chính xác.", "error");
                } else if (status === 403) {
                    swal("Lỗi", "Không có quyền cập nhật mật khẩu của tài khoản khác.", "error");
                } else if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản.", "error");
                } else {
                    swal("Lỗi", "Something went wrong!", "error");
                }
                return;
            }

            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        swal("Thông báo", "Mật khẩu của bạn đã được cập nhật thành công.", "success");

    }).validate({
        rules: {
            oldPassword: "required",
            newPassword: {
                required: true,
                minlength: 6
            },
            confirmPassword: {
                equalTo: "#newPassword"
            }
        },
        messages: {
            oldPassword: "Nhập mật khẩu cũ để xác nhận.",
            newPassword: {
                required: "Mật khẩu mới không được để trống.",
                minlength: "Mật khẩu mới phải có ít nhất 6 kí tự."
            },
            confirmPassword: {
                equalTo: "Mật khẩu xác nhận phải trùng với mật khẩu mới."
            }
        },
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });
    // ====

    // Danh sách yêu thích
    $("#v-pills-watchList-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getWatchListPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/watchlist`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await getWatchListPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
            } else if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền xem watch list của tài khoản khác.", "error");
                } else if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }
            } else {
                swal("Lỗi", "Oops! Something went wrong!", "error");
            }
            return;
        }

        let html = watchListItemTemplate(doneData);
        $("#watchList").empty().append(html);
    });

    $("#watchList").on("click", "button[data-type='details']", async function (event) {
        let id = this.dataset.id;
        window.open(`${config.contextPath}/bidding.html?product-id=${id}`, "_blank");
    }).on("click", "button[data-type='delete']", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let id = this.dataset.id;
        let deletePromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/watchlist/${id}`,
                headers: { "x-access-token": accessToken },
                type: "delete",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await deletePromise;
        } catch (errorData) {
            console.log(errorData);
            swal("Oops!", "Something went wrong!", "error");
            return;
        }

        $(`#watchListRow_${id}`).remove();
    });
    // ====

    // Danh sách sản phẩm đang tham gia đấu giá.
    $("#v-pills-biddingProduct-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/biddingproduct`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let products;
        try {
            products = await getPromise;
        } catch (errorData) {
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
            } else if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền xem của tài khoản khác.", "error");
                } else if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }
            } else {
                swal("Lỗi", "Oops! Something went wrong!", "error");
            }
            return;
        }

        let userId = localStorage.getItem("userId");
        for (let product of products) {
            if (product.highestBiddingEntry && product.highestBiddingEntry.bidder === userId) {
                product.isHighest = true;
            } else {
                product.isHighest = false;
            }
        }
        let html = biddingProductTemplate(products);
        $("#biddingProduct").empty().append(html);
    });

    $("#biddingProduct").on("click", "button[data-type='details']", async function (event) {
        let id = this.dataset.id;
        window.open(`${config.contextPath}/bidding.html?product-id=${id}`, "_blank");
    });
    // ====

    // Danh sách sản phẩm thắng đấu giá
    $("#v-pills-winningProducts-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/winningproduct`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await getPromise;
        } catch (errorData) {
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền xem của tài khoản khác.", "error");
                } else if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }

                return;
            }

            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        let html = winningProductTemplate(doneData);
        $("#winningProduct").empty().append(html);
    });

    $("#winningProduct").on("click", "button[data-type='details']", async function (event) {
        let id = this.dataset.id;
        window.open(`${config.contextPath}/bidding.html?product-id=${id}`, "_blank");
    }).on("click", "button[data-type='review']", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let productId = this.dataset.id;
        let getPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/review/${productId}/seller`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await getPromise;
        } catch (errorData) {
            console.log(errorData);
        }

        if (doneData != undefined) {
            if (doneData.isOK === true) {
                document.getElementById("sellerReview-up").checked = true;
            }

            if (doneData.isOK === false) {
                document.getElementById("sellerReview-down").checked = true;
            }

            document.getElementById("sellerReview-comment").value = doneData.comment;
        } else {
            document.getElementById("sellerReview-up").checked = false;
            document.getElementById("sellerReview-down").checked = false;
            document.getElementById("sellerReview-comment").value = "";
        }

        document.getElementById("saveSellerReview-button").dataset.id = productId;
        $("#sellerReview-modal").modal("show");
    });

    $("#saveSellerReview-button").on("click", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let productId = this.dataset.id;
        let setPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");

            let data = {};
            if (document.getElementById("sellerReview-up").checked === true) {
                data.isOK = true;
            } else {
                data.isOK = false;
            }
            data.comment = document.getElementById("sellerReview-comment").value;

            $.ajax({
                url: `${config.apiPath}/user/${userId}/review/${productId}/seller`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify(data)
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        try {
            await setPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 422) {
                    swal("Lỗi", "Bạn không được phép thay đổi đánh giá.", "error");
                }
                return;
            }

            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        swal({
            title: "OK",
            text: "Người bán đã được đánh giá.",
            icon: "success",
            button: false,
            timer: 2000
        });

        $("#sellerReview-modal").modal("hide");
    });
    // ====

    // Xem danh sách review
    $("#v-pills-review-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/review`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await getPromise;
        } catch (errorData) {
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                swal("Lỗi", "Oops! Something went wrong!", "error");
                return;
            }
        }

        let html = reviewTemplate(doneData);
        $("#review").empty().append(html);
    });
    // ====

    // Thêm sản phẩm
    $("#v-pills-addNewProduct-tab").on("show.bs.tab", async function (event) {
        let getPromise = new Promise((resolve, reject) => {
            $.ajax({
                url: `${config.apiPath}/category`,
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await getPromise;
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let html = "";
        for (let category of doneData) {
            html += `<option value=${category._id}>${category.name}</option>`;
        }

        $("#addNewProduct-category").empty().append(html);
    });

    $("#addNewProduct-images").on("change", async function (event) {
        let fileList = $("#fileList");
        let html = "";
        for (let file of this.files) {
            html += `<li class="list-group-item d-flex justify-content-between align-items-center">${file.name}<span class="badge badge-primary badge-pill">${formatter.numberFormatter.format(file.size / 1024 / 1024)} MiB</span></li>`;
        }
        fileList.empty().append(html);
    });

    let inputmaskForPriceOptions = {
        alias: "numeric",
        autoUnmask: true,
        radixPoint: ",",
        groupSeparator: ".",
        rightAlign: false,
        autoGroup: true
    };

    $("#addNewProduct-startPrice").inputmask(inputmaskForPriceOptions);
    $("#addNewProduct-priceStep").inputmask(inputmaskForPriceOptions);
    $("#addNewProduct-takeoverPrice").inputmask(inputmaskForPriceOptions);
    $("#addNewProduct-endTime").datetimepicker();

    $("#addNewProduct-description").summernote({
        placeholder: "Mô tả cho sản phẩm",
        tabsize: 2,
        height: 200
    });

    $("#addNewProduct-form").on("submit", async function (event) {
        event.preventDefault();

        if (!$(this).valid()) {
            return;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        if (this.elements["images"].files.length > 3) {
            swal("Thông báo", "Số lượng ảnh tối đa là 3.", "warning");
            return;
        }

        let formData = new FormData();
        formData.set("name", $("#addNewProduct-name").val());
        formData.set("category", $("#addNewProduct-category").val());
        for (let file of this.elements["images"].files) {
            formData.append("images", file);
        }
        formData.set("startPrice", $("#addNewProduct-startPrice").val());
        formData.set("priceStep", $("#addNewProduct-priceStep").val());
        if ($("#addNewProduct-takeoverPrice").val() !== "") {
            formData.set("takeoverPrice", $("#addNewProduct-takeoverPrice").val());
        }
        formData.set("endTime", $("#addNewProduct-endTime").val());
        formData.set("description", $("#addNewProduct-description").val());
        formData.set("timeExtension", $("#addNewProduct-timeExtension").prop("checked"));

        let postPromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/product`,
                headers: { "x-access-token": accessToken },
                method: "post",
                data: formData,
                contentType: false,
                processData: false,
                timeout: config.ajaxTimeout,
                dataType: "json"
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await postPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền đăng sản phẩm.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }

                return;
            }

            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        swal({
            title: "OK",
            text: "Sản phẩm đã được hệ thống ghi nhận.",
            icon: "success",
            button: true,
            timer: 3000
        });
    }).validate({
        rules: {
            name: "required",
            category: "required",
            startPrice: "required",
            priceStep: "required",
            endTime: "required"
        },
        messages: {
            name: "Tên sản phẩm không được để trống.",
            category: "Sản phẩm phải thuộc một danh mục cụ thể.",
            startPrice: "Giá khởi điểm không được để trống.",
            priceStep: "Bước giá không được để trống.",
            endTime: "Thời gian kết thúc không được để trống."
        },
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });
    // ====

    // Danh sách sản phẩm đang bán
    $("#v-pills-sellingProduct-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/sellingproduct`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await getPromise;
        } catch (errorData) {
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền xem của tài khoản khác.", "error");
                } else if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }

                return;
            }

            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        let html = sellingProductTemplate(doneData);
        $("#sellingProduct").empty().append(html);
    });

    $("#sellingProduct").on("click", "button[data-type='details']", async function (event) {
        let id = this.dataset.id;
        window.open(`${config.contextPath}/bidding.html?product-id=${id}`, "_blank");
    });
    // ====

    // Danh sách sản phẩm đã có người mua
    $("#v-pills-soldProduct-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/soldproduct`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await getPromise;
        } catch (errorData) {
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền xem của tài khoản khác.", "error");
                } else if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản.", "error");
                } else {
                    swal("Lỗi", "Oops! Something went wrong!", "error");
                }

                return;
            }

            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        let html = soldProductTemplate(doneData);
        $("#soldProduct").empty().append(html);
    });

    $("#soldProduct").on("click", "button[data-type='details']", async function (event) {
        let id = this.dataset.id;
        window.open(`${config.contextPath}/bidding.html?product-id=${id}`, "_blank");
    }).on("click", "button[data-type='review']", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let productId = this.dataset.id;
        let getPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/review/${productId}/winner`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let doneData;
        try {
            doneData = await getPromise;
        } catch (errorData) {
            console.log(errorData);
        }

        if (doneData != undefined) {
            if (doneData.isOK === true) {
                document.getElementById("winnerReview-up").checked = true;
            }

            if (doneData.isOK === false) {
                document.getElementById("winnerReview-down").checked = true;
            }

            document.getElementById("winnerReview-comment").value = doneData.comment;
        } else {
            document.getElementById("winnerReview-up").checked = false;
            document.getElementById("winnerReview-down").checked = false;
            document.getElementById("winnerReview-comment").value = "";
        }

        document.getElementById("saveWinnerReview-button").dataset.id = productId;
        $("#winnerReview-modal").modal("show");
    });

    $("#saveWinnerReview-button").on("click", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let productId = this.dataset.id;
        let setPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let accessToken = localStorage.getItem("accessToken");

            let data = {};
            if (document.getElementById("winnerReview-up").checked === true) {
                data.isOK = true;
            } else {
                data.isOK = false;
            }
            data.comment = document.getElementById("winnerReview-comment").value;

            $.ajax({
                url: `${config.apiPath}/user/${userId}/review/${productId}/winner`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify(data)
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        try {
            await setPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 422) {
                    swal("Lỗi", "Bạn không được phép thay đổi đánh giá.", "error");
                }
                return;
            }

            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        swal({
            title: "OK",
            text: "Người mua đã được đánh giá.",
            icon: "success",
            button: false,
            timer: 2000
        });

        $("#sellerReview-modal").modal("hide");
    });
    // ====

    // Quản lý yêu cầu nâng cấp tài khoản thành seller.
    $("#v-pills-upgradeRequest-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/admin/upgrade-request`,
                headers: { "x-access-token": accessToken },
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let users;
        try {
            users = await getPromise;
        } catch (errorData) {
            console.log(errorData);
            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        let html = upgradeRequestListTemplate(users);
        $("#upgradeRequest").empty().append(html);
    });

    $("#upgradeRequest").on("click", "button[data-type='accept-request']", async function (event) {
        let userId = this.dataset.id;
        let putPromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/admin/upgrade-request/${userId}`,
                headers: { "x-access-token": accessToken },
                method: "put",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let user;
        try {
            user = await putPromise;
        } catch (errorData) {
            console.log(errorData);
            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        $(`#upgradeRequestRow_${userId}`).remove();
        swal({ title: "OK", text: "Đã duyệt yêu cầu", icon: "success", timer: 1000 });
    }).on("click", "button[data-type='delete-request']", async function (event) {
        let userId = this.dataset.id;
        let deletePromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/admin/upgrade-request/${userId}`,
                headers: { "x-access-token": accessToken },
                method: "delete",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let user;
        try {
            user = await deletePromise;
        } catch (errorData) {
            console.log(errorData);
            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        $(`#upgradeRequestRow_${userId}`).remove();
        swal({ title: "OK", text: "Đã xóa yêu cầu", icon: "success", timer: 1000 });
    });
    // ====

    // Quản lý người dùng
    let findUserPage = 1;
    $("#findUserForm").on("submit", async function (event) {
        event.preventDefault();

        findUserPage = 1;
        let query = {
            page: findUserPage
        };

        let userName = this.elements["userName"].value;

        if (userName) {
            query.userName = userName;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user`,
                headers: { "x-access-token": accessToken },
                type: "get",
                dataType: "json",
                timeout: config.ajaxTimeout,
                data: query
            }).done((data) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let data;
        try {
            data = await getPromise;
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let htmlRows = userListRowTemplate(data.users);
        let htmlTable = userListTemplate(htmlRows);

        $("#userList").empty().append(htmlTable);

        if (data.hasMore) {
            document.getElementById("loadMoreUsers").hidden = false;
        }
    });

    $("#loadMoreUsers").on("click", async function (event) {
        findUserPage += 1;
        let query = {
            page: findUserPage
        };

        let userName = document.getElementById("findUserForm").elements["userName"].value;

        if (userName) {
            query.userName = userName;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user`,
                headers: { "x-access-token": accessToken },
                type: "get",
                dataType: "json",
                timeout: config.ajaxTimeout,
                data: query
            }).done((data) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let data;
        try {
            data = await getPromise;
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let htmlRows = userListRowTemplate(data.users);

        $("#userListTableBody").append(htmlRows);

        if (!data.hasMore) {
            this.hidden = true;
        }
    });

    $("#userList").on("click", "button[data-type='reset-password-user']", async function (event) {
        document.getElementById("resetPasswordModal-Save").dataset.id = this.dataset.id;
        $("#resetPasswordForm").validate().resetForm();
        document.getElementById("resetPasswordForm-confirm").value = "";
        document.getElementById("resetPasswordForm-password").value = "";
        $("#resetPasswordModal").modal("show");
    });

    $("#userList").on("click", "button[data-type='delete-user']", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let userId = this.dataset.id;
        let deletePromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}`,
                headers: { "x-access-token": accessToken },
                method: "delete",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        try {
            await deletePromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        $(`#userRow_${userId}`).remove();

        swal({
            title: "OK",
            text: "Tài khoản đã được xóa.",
            icon: "success",
            timer: 1000
        });
    });

    $("#resetPasswordModal-Save").on("click", async function (event) {
        if (!$("#resetPasswordForm").valid()) {
            return;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let data = {
            password: document.getElementById("resetPasswordForm-confirm").value
        };

        let postPromise = new Promise((resolve, reject) => {
            let userId = this.dataset.id;
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/password-reset`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify(data)
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });
        try {
            await postPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;

                if (status === 403) {
                    swal("Lỗi", "Không có quyền cập nhật mật khẩu của tài khoản khác.", "error");
                    return;
                }

                if (status === 404) {
                    swal("Lỗi", "Không tìm thấy tài khoản.", "error");
                    return;
                }

                swal("Lỗi", "Something went wrong!", "error");
                return;
            }

            swal("Lỗi", "Something went wrong!", "error");
            return;
        }

        $("#resetPasswordModal").modal("hide");
        swal("Thông báo", "Mật khẩu của người dùng đã được cập nhật thành công.", "success");
    });

    $("#resetPasswordForm").on("submit", async function (event) {
        event.preventDefault();

        $("#resetPasswordModal-Save").trigger("click");
    }).validate({
        rules: {
            password: {
                required: true,
                minlength: 6
            },
            confirm: {
                equalTo: "#resetPasswordForm-password"
            }
        },
        messages: {
            password: {
                required: "Mật khẩu mới không được để trống.",
                minlength: "Mật khẩu mới phải có ít nhất 6 kí tự."
            },
            confirm: {
                equalTo: "Mật khẩu xác nhận phải trùng với mật khẩu mới."
            }
        },
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });
    // ====

    // Quản lý danh mục
    $("#v-pills-categoryManagement-tab").on("show.bs.tab", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let getPromise = new Promise((resolve, reject) => {
            $.ajax({
                url: `${config.apiPath}/category`,
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let categories;
        try {
            categories = await getPromise;
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let html = categoryListTemplate(categories);
        $("#categoryList").empty().append(html);
    });

    $("#submitCategory-form").on("submit", async function (event) {
        event.preventDefault();

        if (!$(this).valid()) {
            return;
        }

        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let postPromise = new Promise((resolve, reject) => {
            let data = {
                name: this.elements["name"].value
            };
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/category`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify(data)
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let newCategory;
        try {
            newCategory = await postPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền tạo danh mục.", "error");
                    return;
                }

                if (status === 422) {
                    swal("Lỗi", "Danh mục muốn tạo có tên trùng với một danh mục có sẵn.", "warning");
                    return;
                }

                swal("Lỗi", "Oops! Something went wrong!", "error");
                return;
            }
            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        swal({
            title: "OK",
            text: "Danh mục đã được tạo mới.",
            icon: "success",
            timer: 1000
        });

        $("#v-pills-categoryManagement-tab").trigger("show.bs.tab");
    }).validate({
        rules: {
            name: "required"
        },
        messages: {
            name: "Tên danh mục không được để trống."
        },
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });

    $("#categoryList").on("click", "button[data-type='edit-category']", async function (event) {
        let categoryId = this.dataset.id;
        document.getElementById("editCategory-modal-save").dataset.id = categoryId;
        document.getElementById("editCategoryForm").elements["name"].value = document.getElementById(`categoryRow_${categoryId}`).firstElementChild.textContent;
        $("#editCategory-modal").modal("show");
    });

    $("#categoryList").on("click", "button[data-type='delete-category']", async function (event) {
        let categoryId = this.dataset.id;
        let deletePromise = new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/category/${categoryId}`,
                headers: { "x-access-token": accessToken },
                method: "delete",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let deletedCategory;
        try {
            deletedCategory = await deletePromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền xóa danh mục.", "error");
                    return;
                }

                swal("Lỗi", "Oops! Something went wrong!", "error");
                return;
            }

            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        swal({
            title: "OK",
            text: "Danh mục đã được xóa.",
            icon: "success",
            timer: 1000
        });
        $(`#categoryRow_${categoryId}`).remove();
    });

    $("#editCategoryForm").on("submit", async function (event) {
        event.preventDefault();

        if (!$(this).valid()) {
            return;
        }

        $("#editCategory-modal-save").trigger("click");
    }).validate({
        rules: {
            name: "required"
        },
        messages: {
            name: "Tên danh mục không được để trống."
        },
        errorElement: "div",
        errorClass: "invalid-feedback",
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid");
        }
    });

    $("#editCategory-modal-save").on("click", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            return;
        }

        let categoryId = this.dataset.id;

        let putPromise = new Promise((resolve, reject) => {
            let data = {
                name: document.getElementById("editCategoryForm").elements["name"].value
            };
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/category/${categoryId}`,
                headers: { "x-access-token": accessToken },
                method: "put",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify(data)
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let editedCategory;
        try {
            editedCategory = await putPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 403) {
                    swal("Lỗi", "Bạn không có quyền tạo danh mục.", "error");
                    return;
                }

                if (status === 422) {
                    swal("Lỗi", "Tên danh mục muốn thay đổi bị trùng với một danh mục có sẵn.", "warning");
                    return;
                }

                swal("Lỗi", "Oops! Something went wrong!", "error");
                return;
            }

            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        swal({
            title: "OK",
            text: "Danh mục đã được chỉnh sửa.",
            icon: "success",
            timer: 1000
        });

        $("#editCategory-modal").modal("hide");
        $("#v-pills-categoryManagement-tab").trigger("show.bs.tab");
    });
    // ====
});
