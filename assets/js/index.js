import * as config from "./utils/config.js";
import * as checker from "./utils/checker.js";
import * as helper from "./utils/handlebars-helper.js";

$(async function () {
    Handlebars.registerHelper("formatCurrency", helper.formatCurrency);
    Handlebars.registerHelper("formatDateTime", helper.formatDateTime);
    Handlebars.registerHelper("formatNumber", helper.formatNumber);
    Handlebars.registerHelper("math", helper.math);
    Handlebars.registerHelper("isNew", helper.isNew);
    Handlebars.registerHelper("timeLeft", helper.timeLeft);
    Handlebars.registerHelper("getImageByIndex", helper.getImageByIndex);
    Handlebars.registerHelper("buildAbsoluteImagePath", helper.buildAbsoluteImagePath);
    Handlebars.registerHelper("hideStringWithAsterisk", helper.hideStringWithAsterisk);

    let productTemplate = Handlebars.compile($("#product-template").html());
    let detailsTemplate = Handlebars.compile($("#details-template").html());

    $("#brand").attr("href", config.contextPath);

    let user = JSON.parse(localStorage.getItem("user"));

    $("#userQuickLink-profile").on("click", function (event) {
        window.location.replace(`${config.contextPath}/profile.html`);
    });

    $("#userQuickLink-signin").on("click", function (event) {
        window.location.replace(`${config.contextPath}/sign-in.html`);
    });

    $("#userQuickLink-logout").on("click", function (event) {
        localStorage.clear();
        window.location.reload();
    });

    if (user) {
        document.getElementById("userQuickLink-userName").textContent = user.userName;
        document.getElementById("userQuickLink-profile").hidden = false;
        document.getElementById("userQuickLink-logout").hidden = false;
    } else {
        document.getElementById("userQuickLink-signin").hidden = false;
    }

    // Get category.
    $.ajax({
        url: `${config.apiPath}/category`,
        method: "get",
        dataType: "json",
        timeout: config.ajaxTimeout
    }).done((data) => {
        let listDanhMuc = $("#listDanhMuc");
        $.each(data, (index, category) => {
            let newEntry = $("<a></a>").addClass("dropdown-item").text(category.name);
            listDanhMuc.append(newEntry);
        });
    });
    // ====

    // 5 sản phẩm có nhiều lượt ra giá nhất.
    $("#v-pills-top5-nhieuLuotRaGiaNhat-tab").on("show.bs.tab", async function (event) {
        $.ajax({
            url: `${config.apiPath}/product`,
            method: "get",
            data: { searchType: "top5-1" }, // Tự động chuyển về query string.
            dataType: "json",
            timeout: config.ajaxTimeout
        }).done((products) => {
            let html = productTemplate(products);
            $("#top5-nhieuLuotRaGiaNhat").empty().append(html);
        });
    });
    // ====

    // 5 sản phẩm có giá cao nhất.
    $("#v-pills-top5-giaCaoNhat-tab").on("show.bs.tab", async function (event) {
        $.ajax({
            url: `${config.apiPath}/product`,
            method: "get",
            data: { searchType: "top5-2" },
            dataType: "json",
            timeout: config.ajaxTimeout
        }).done((products) => {
            let html = productTemplate(products);
            $("#top5-giaCaoNhat").empty().append(html);
        });
    });
    // ====

    // 5 sản phẩm sắp kết thúc.
    $("#v-pills-top5-ganKetThuc-tab").on("show.bs.tab", async function (event) {
        $.ajax({
            url: `${config.apiPath}/product`,
            method: "get",
            data: { searchType: "top5-3" },
            dataType: "json",
            timeout: config.ajaxTimeout
        }).done((products) => {
            let html = productTemplate(products);
            $("#top5-ganKetThuc").empty().append(html);
        });
    });
    // ====

    // Tìm kiếm
    let currentPage = 1;
    let hasRun = false; // Chỉ load category một lần.

    $("#v-pills-search-tab").on("show.bs.tab", async function (event) {
        if (hasRun) {
            return;
        }

        let getCategoryPromise = new Promise((resolve, reject) => {
            $.ajax({
                url: `${config.apiPath}/category`,
                method: "get",
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((categories) => {
                resolve(categories);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let categories;
        try {
            categories = await getCategoryPromise;
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let html = `<option value="">Tất cả các danh mục</option>`;
        for (let category of categories) {
            html += `<option value=${category._id}>${category.name}</option>`;
        }

        $("#searchForm-category").empty().append(html);
        hasRun = true;
    });

    $("#searchForm").on("submit", async function (event) {
        event.preventDefault();
    });

    $("#loadMore-button").on("click", async function (event) {
        let searchPromise = new Promise((resolve, reject) => {
            let elements = document.getElementById("searchForm").elements;
            let searchOptions = {
                searchType: "custom",
                name: elements.name.value,
                category: elements.category.value,
                sortByEndTime: elements.sortByEndTime.value,
                sortByPrice: elements.sortByPrice.value,
                page: currentPage++
            };

            $.ajax({
                url: `${config.apiPath}/product`,
                method: "get",
                data: searchOptions,
                dataType: "json",
                timeout: config.ajaxTimeout
            }).done((data) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let data;
        try {
            data = await searchPromise;
        } catch (errorData) {
            console.log(errorData);
            currentPage--;
            return;
        }

        let html = productTemplate(data.products);
        $("#searchResult").append(html);

        this.textContent = "Tiếp ...";
        $("#resetSearch-button").prop("hidden", false);

        $("#searchForm-name").prop("disabled", true);
        $("#searchForm-category").prop("disabled", true);
        $("#searchFrom-sortByEndTime").prop("disabled", true);
        $("#searchFrom-sortByPrice").prop("disabled", true);

        if (!data.hasMore) {
            this.hidden = true;
        }
    });

    $("#resetSearch-button").on("click", async function (event) {
        $("#searchResult").empty();

        $("#searchForm-name").prop("disabled", false);
        $("#searchForm-category").prop("disabled", false);
        $("#searchFrom-sortByEndTime").prop("disabled", false);
        $("#searchFrom-sortByPrice").prop("disabled", false);

        currentPage = 1;
        $("#v-pills-search-tab").trigger("show.bs.tab");

        this.hidden = true;
        $("#loadMore-button").prop("hidden", false);
        $("#loadMore-button").text("Tìm kiếm");
    });
    // ====

    // Product card
    $("#v-pills-tabContent").on("click", "button[data-button-type='details']", function (event) {
        // Không dùng arrow function callback vì this sẽ trỏ tới parent (document) ở ngoài.
        let button = $(this);
        let id = button.data("id");
        $.ajax({
            url: `${config.apiPath}/product/${id}`,
            method: "get",
            dataType: "json",
            timeout: config.ajaxTimeout
        }).done((product) => {
            let html = detailsTemplate(product);
            $("#details-modal-content").empty().append(html);
            $("#details-modal").modal();
        });
    });

    $("#v-pills-tabContent").on("click", "button[data-button-type='watchlist']", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let postPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let productId = this.dataset.id;
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/watchlist`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify({ productId: productId })
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let product;
        try {
            product = await postPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 422) {
                    swal("Thông báo", "Sản phẩm đã tồn tại trong watch list.", "success");
                    return;
                }

                swal("Lỗi", "Oops! Something went wrong!", "error");
                return;
            }
            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        swal({
            title: "Watch List",
            text: "Đã thêm sản phẩm thành công.",
            icon: "success",
            timer: 1000
        });
    });
    // ====

    // Details modal
    $("#details-modal").on("click", "#button-go-to-bid", async function (event) {
        let id = this.dataset.id;
        window.open(`${config.contextPath}/bidding.html?product-id=${id}`, "_blank");
    });

    $("#details-modal").on("click", "#button-add-to-watch-list", async function (event) {
        try {
            await checker.reCheckAuthentication();
        } catch (errorData) {
            console.log(errorData);
            return;
        }

        let postPromise = new Promise((resolve, reject) => {
            let userId = localStorage.getItem("userId");
            let productId = this.dataset.id;
            let accessToken = localStorage.getItem("accessToken");
            $.ajax({
                url: `${config.apiPath}/user/${userId}/watchlist`,
                headers: { "x-access-token": accessToken },
                method: "post",
                dataType: "json",
                timeout: config.ajaxTimeout,
                contentType: "application/json",
                data: JSON.stringify({ productId: productId })
            }).done((data, textStatus, xhr) => {
                resolve(data);
            }).fail((xhr, textStatus, errorThrown) => {
                reject({ xhr: xhr, textStatus: textStatus, errorThrown: errorThrown });
            });
        });

        let product;
        try {
            product = await postPromise;
        } catch (errorData) {
            console.log(errorData);
            if (errorData.textStatus === "timeout") {
                swal("Lỗi", "Hiện tại kết nối tới máy chủ không thể thiết lập.", "error");
                return;
            }

            if (errorData.textStatus === "error") {
                let status = errorData.xhr.status;
                if (status === 422) {
                    swal("Thông báo", "Sản phẩm đã tồn tại trong watch list.", "success");
                    return;
                }

                swal("Lỗi", "Oops! Something went wrong!", "error");
                return;
            }
            swal("Lỗi", "Oops! Something went wrong!", "error");
            return;
        }

        swal({
            title: "Watch List",
            text: "Đã thêm sản phẩm thành công.",
            icon: "success",
            timer: 1000
        });
    });

    $("#details-modal").on("click", "button[data-type='public-profile']", function (event) {
        let userId = this.dataset.id;
        window.open(`${config.contextPath}/public-profile.html?user-id=${userId}`, "_blank");
    });
    // ====
});
